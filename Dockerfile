ARG LICENSE_FINDER_VERSION=5.6.2

FROM licensefinder/license_finder:$LICENSE_FINDER_VERSION
MAINTAINER GitLab

ARG LICENSE_FINDER_VERSION
ENV LICENSE_FINDER_VERSION $LICENSE_FINDER_VERSION

# Install JDK 11
RUN cd /tmp && \
    wget --quiet --no-cookies https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.2%2B9/OpenJDK11U-jdk_x64_linux_hotspot_11.0.2_9.tar.gz -O jdk-11.tgz && \
    tar xf /tmp/jdk-11.tgz && \
    mv jdk-11.0.2+9 /usr/lib/jvm/adoptopen_jdk11 && \
    rm /tmp/jdk-11.tgz

RUN npm install npm-install-peers cheerio

# Don't let Rubygem fail with the numerous projects using PG or MySQL, install realpath
RUN apt-get update && apt-get install -y libpq-dev libmysqlclient-dev realpath && rm -rf /var/lib/apt/lists/*

# Don't load RVM automatically, it doesn't work with GitLab-CI
RUN mv /etc/profile.d/rvm.sh /rvm.sh

COPY test /test
COPY run.sh html2json.js /

ENTRYPOINT ["/run.sh"]
